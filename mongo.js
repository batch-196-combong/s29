// db.products.insertMany(
// [
//     {
//         "name": "Iphone X",
//         "price": 30000,
//         "isActive": true
//     },
//     {
//         "name": "Samsung Galaxy S21",
//         "price": 51000,
//         "isActive": true
//     },
//     {
//         "name": "Razer Blackshark V2X",
//         "price": 2800,
//         "isActive": false
//     },
//     {
//         "name": "RAKK Gaming House",
//         "price": 1800,
//         "isActive": true
//     },
//     {
//         "name": "IRazer Mechanical Keyboard",
//         "price": 4000,
//         "isActive": true
//     }
// ])
// 

// Query Operators allows us to expand our queries and define 
// conditions instead of just looking specififc values

// $gt, $lt, $gte, $lte
// db.products.find({price:{$gt:3000}})
// db.products.find({price:{$lt:3000}})
// db.products.find({price:{$gte:30000}})
// db.products.find({price:{$lte:2800}})

// db.users.insertMany(
//     [
//         {
//             "firstname": "Mary Jane",
//             "lastname": "Watson",
//             "email": "mjtiger@gmail.com",
//             "password": "tigerjackpot15",
//             "isAdmin": false
//         },
//         {
//             "firstname": "Gwen",
//             "lastname": "Stacy",
//             "email": "stacyTech@gmail.com",
//             "password": "stacyTech1991",
//             "isAdmin": true
//         },
//         {
//             "firstname": "Peter",
//             "lastname": "Parker",
//             "email": "peterWebDev@gmail.com",
//             "password": "webdeveloperPeter",
//             "isAdmin": true
//         },
//         {
//             "firstname": "Jonah",
//             "lastname": "Jameson",
//             "email": "jjjameson@gmail.com",
//             "password": "spideyisamence",
//             "isAdmin": false
//         },
//         {
//             "firstname": "Otto",
//             "lastname": "Octavius",
//             "email": "ottoOctopi@gmail.com",
//             "password": "docock15",
//             "isAdmin": true
//         }
//     ]
//     )

// $regex - query that will allow us to find documents which will match
// the characters/ pattern of the characters we are looking for.

// db.users.find({firstname:{$regex: 'O'}})
// 
// // $options - used our regex will be case insensitive
// db.users.find({firstname:{$regex:'O', $options: '$i'}})
// 
// db.products.find({name:{$regex:'phone', $options: '$i'}})

// $or $and - logical operators - works alost the same way as they in JS
// db.products.find({$or:[{name:{$regex:'x', $options:'$i'}},{price:{$lte:10000}}]})
db.products.find({$and:[{name:{$regex:'razer', $options:'$i'}},{price:{$gte:3000}}]})